using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.IO.Compression; //insurance cards
using BitMiracle.LibTiff.Classic;
using BitMiracle.TiffCP;
using BitMiracle.Tiff2Pdf;
using iTextSharp.text.pdf;  //defines PDFReader
using iTextSharp.text; //defines document
using System.Drawing; //bmp conversion
using System.Drawing.Imaging; //bmp conversion
using System.Xml.Xsl;
using System.Xml;
using System.Xml.XPath;
using System.Text;
using System.Xml.Linq;
using System.Runtime.ExceptionServices;  //comes with .NET4

/* Processes patient_images, HIE documents, insurance cards and patient pictures
 * essentially anything that needs a database call
 * Patient_images are stored as individual pages. We combine those into a PDF for display
 * HIE, insurance cards and patient pictures are stored as blobs in the database
 * 
 * 08/18/2015 - jbehrens - added insurance cards and patient pictures
 * 12/30/2015 - jbehrens - added WestShore Cardiology datasources
 * 
 * calls
 * http://localhost:60495/default.aspx?region=00003&imageid=87759AD7-9745-4A3C-9E8D-91FFF55D2161&filetype=c insurance card
 * http://localhost:60495/default.aspx?region=00003&imageid=BC7D7D9C-8DE1-4461-855C-6DB889CEBD29&filetype=h HIE document
 * http://localhost:60495/default.aspx?region=00003&imageid=37BDA423-11D9-4F0B-9B04-1A0DE5CA6CA3 patient_images
 * 
 */
namespace TIFFPDF
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Impersonate the app pool service account
            WindowsImpersonationContext impersonationcontext;
            impersonationcontext = WindowsIdentity.Impersonate(System.IntPtr.Zero);

            Response.AppendHeader("Access-Control-Allow-Origin", "*");

            String region = "";
            String imageid = "";

            if (Request.QueryString.Count == 2)
            {
                region = Request.QueryString[0];
                imageid = Request.QueryString[1];
                GetFiles(region, imageid);
            }

            if (Request.QueryString.Count == 3)
            {
                region = Request.QueryString[0];
                imageid = Request.QueryString[1];

                if (Request.QueryString[2].ToString() == "c") //insurance card
                {
                    GetPatientImage(region, imageid);
                }
                else if (Request.QueryString[2].ToString() == "test") //HIE --test .
                {
                     CompiledTransformQueried(region, imageid);
                }
                else //HIE --expecting an H I think, make this the default if doc_type is supplied.
                {
                     CompiledTransformQueried_temp(region, imageid);
                }
            }

        }

        //sets up the database connection string based on the enterprise_id passed in
        private static string GetConnectionString(String region)
        {
            String connectionstring = "";
            switch (region)
            {
                case "00001":
                    connectionstring = Properties.Settings.Default.DB00001;
                    break;
                case "00002":
                    connectionstring = Properties.Settings.Default.DB00002;
                    break;
                case "00003":
                    connectionstring = Properties.Settings.Default.DB00003;
                    break;
                case "00004":
                    connectionstring = Properties.Settings.Default.DB00004;
                    break;
                case "00005":
                    connectionstring = Properties.Settings.Default.DB00005;
                    break;
                case "00006":
                    connectionstring = Properties.Settings.Default.DB00006;
                    break;
                case "00007":
                    connectionstring = Properties.Settings.Default.DB00007;
                    break;
                case "WestShore":
                    connectionstring = Properties.Settings.Default.DBWestShore;
                    break;
            }
            return connectionstring;
        }

        //patient_images
        private void GetFiles(String region, String imageid)
        {

            String query = "SELECT d.document_id,COALESCE(location + sub_location, location) + g.file_name AS unc_path, right(g.file_name,3) ";
            query += "FROM document d with(nolock) LEFT OUTER JOIN patient_ics_images i with(nolock) ON d.document_id = i.document_id ";
            query += "AND d.practice_id = i.practice_id and d.enterprise_id = i.enterprise_id ";
            query += "INNER JOIN page g with(nolock) ON g.document_id = d.document_id ";
            query += "where d.document_id = '";
            query += imageid;
            query += "'";

            //'62D25DCA-301A-4F58-9E92-A65101162DEB'";
            //'BDEE1BE2-CBBE-4D3E-83D1-04E81062F8F8'";


            String connectionstring = GetConnectionString(region);
            SqlConnection connection = new SqlConnection(connectionstring);
            SqlDataAdapter da = new SqlDataAdapter(query, connection);
            DataSet ds = new DataSet();
            da.Fill(ds, "Files");

            string[] fileNames = new string[ds.Tables["Files"].Rows.Count];


            String filename = "";
            String filenametemp = "";
            String format = "";
            int i = 0;
            foreach (DataRow row in ds.Tables["Files"].Rows)
            {
                filename = row[0].ToString();
                filenametemp = row[1].ToString().ToUpper();
                filenametemp = filenametemp.Replace("\\\\MHSSA061\\", "\\\\MHSSA061.ONE.ADS.CHE.ORG\\"); //R7
                filenametemp = filenametemp.Replace("\\\\MSH1APTNGW\\", "\\\\MSH1APTNGW.MS.TRINITY-HEALTH.ORG\\"); //WestShore Cardiology
                fileNames[i] = filenametemp;
                format = row[2].ToString();
                i++;
            }

            //System.Web.HttpServerUtility.Server.MapPath("~/Temp/")

            string randomName = DateTime.Now.Ticks.ToString();
            string NewFileName = Server.MapPath("~/Temp/") + randomName + ".tif";
            String outfile = Server.MapPath("~/Temp/") + filename + ".pdf";

            switch (format.ToLower())
            {
                case "tif":
                    Array.Resize(ref fileNames, i + 1);
                    fileNames[i] = NewFileName;
                    MergeTiffs(fileNames, NewFileName, outfile);
                    break;
                case "pdf":
                    CombineMultiplePDFs(fileNames, outfile);
                    break;
            }

        }

        //HIE
        private void CompiledTransformQueried(String region, String imageid)
        {
            String query = "SELECT cast(convert(varbinary(max),h.content) as varchar(max)), convert(xml,convert(varbinary(max),xsl.content)) ";
            query += "FROM hie_document h with(nolock) ";
            // query += "join provider_mstr pm with(nolock) on h.provider_id = pm.provider_id ";
            query += "join hie_xsl xsl with(nolock) on h.xsl_id = xsl.uniq_id ";
            // query += "join person p with(nolock) on h.person_id = p.person_id ";
            query += "WHERE document_id = '";
            query += imageid;
            query += "'";

            String connectionstring = GetConnectionString(region);
            SqlConnection connection = new SqlConnection(connectionstring);
            SqlDataAdapter da = new SqlDataAdapter(query, connection);
            DataSet ds = new DataSet();
            da.Fill(ds, "Files");

            String xmlData = "";
            String xsl = "";
            foreach (DataRow row in ds.Tables["Files"].Rows)
            {
                xmlData = row[0].ToString();
                xsl = row[1].ToString();
            }

            try
            {
                // Create the XmlReader object.
                XmlReader reader = XmlReader.Create(new StringReader(xmlData));

                string xslPath = Server.MapPath("~/Temp/");

                //try
                //{
                //    using (StreamReader sr = new StreamReader(xslPath + "CCD.xsl"))
                //    {
                //        xsl = sr.ReadToEnd();
                //    }
                //}
                //catch (Exception e)
                //{
                //    Response.Write(e.Message);
                //}

                // Create and load the transform with script execution enabled.
                XslCompiledTransform transform = new XslCompiledTransform();
                XsltSettings settings = new XsltSettings();
                settings.EnableScript = true;
                settings.EnableDocumentFunction = true;

                XmlDocument xsldoc = new XmlDocument();
                xsldoc.LoadXml(xsl);
                transform.Load(xsldoc, settings, null);


                //// Create a writer for writing the transformed file.
                XmlWriter writer = XmlWriter.Create(Response.OutputStream, transform.OutputSettings);

                //// Execute the transformation.
                transform.Transform(reader, writer);
            }
            catch (Exception e)
            {
                //Response.Write(e  + "<br>");
                throw new IOException();
            }
        }

        //HIE
        private void CompiledTransformQueried_temp(String region, String imageid)
        {
            String query = "SELECT cast(convert(varbinary(max),h.content) as varchar(max)), convert(xml,convert(varbinary(max),xsl.content)) ";
            query += "FROM hie_document h with(nolock) ";
            // query += "join provider_mstr pm with(nolock) on h.provider_id = pm.provider_id ";
            query += "join hie_xsl xsl with(nolock) on h.xsl_id = xsl.uniq_id ";
            // query += "join person p with(nolock) on h.person_id = p.person_id ";
            query += "WHERE document_id = '";
            query += imageid;
            query += "'";

            String connectionstring = GetConnectionString(region);
            SqlConnection connection = new SqlConnection(connectionstring);
            SqlDataAdapter da = new SqlDataAdapter(query, connection);
            DataSet ds = new DataSet();
            da.Fill(ds, "Files");

            String xmlData = "";
            String xsl = "";
            foreach (DataRow row in ds.Tables["Files"].Rows)
            {
                xmlData = row[0].ToString();
                xsl = row[1].ToString();
            }

            try
            {
                // Create the XmlReader object.
                XmlReader reader = XmlReader.Create(new StringReader(xmlData));

                string xslPath = Server.MapPath("~/Temp/");

                try
                {
                    using (StreamReader sr = new StreamReader(xslPath + "CCD.xsl"))
                    {
                        xsl = sr.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    Response.Write(e.Message);
                }



                // Create and load the transform with script execution enabled.
                XslCompiledTransform transform = new XslCompiledTransform();
                XsltSettings settings = new XsltSettings();
                settings.EnableScript = true;
                settings.EnableDocumentFunction = true;

                XmlDocument xsldoc = new XmlDocument();
                xsldoc.LoadXml(xsl);
                transform.Load(xsldoc, settings, null);


                //// Create a writer for writing the transformed file.
                XmlWriter writer = XmlWriter.Create(Response.OutputStream, transform.OutputSettings);

                //// Execute the transformation.
                transform.Transform(reader, writer);
            }
            catch (Exception e)
            {
                Response.Write(e + "<br>");
                throw;
            }
        }

        //Insurance cards and patient photos
        private void GetPatientImage(String region, String imageid)
        {
            String query = "SELECT image_id, cast(convert(varbinary(max),image_data) as varchar(max)) ";
            query += "from images ";
            query += "where image_id = '";
            query += imageid;
            query += "'";

            FileStream fs;                          // Writes the BLOB to a file (*.bmp).
            BinaryWriter bw;                        // Streams the BLOB to the FileStream object.
            int bufferSize = 100;                   // Size of the BLOB buffer.
            byte[] outbyte = new byte[bufferSize];  // The BLOB byte[] buffer to be filled by GetBytes.
            long retval;                            // The bytes returned from GetBytes.
            long startIndex = 0;                    // The starting position in the BLOB output.
            string image_id = "";                     // The employee id to use in the file name.

            // Open the connection and read data into the DataReader.
            String connectionstring = GetConnectionString(region);
            SqlConnection connection = new SqlConnection(connectionstring);
            SqlCommand CcdCommand = new SqlCommand(query, connection);
            
            connection.Open();
            SqlDataReader myReader = null;

            try
            {
                myReader = CcdCommand.ExecuteReader(CommandBehavior.SequentialAccess);
            }
            catch (Exception e)
            {
                Response.Write("GetPatientImage: error running sql " + "<br>");
                Response.Write(e.Message + "<br>");
                Response.Write(e + "<br>");
            }

            string extractPath = Server.MapPath("~/Temp/");

            while (myReader.Read())
            {
                // Get the employee id, which must occur before getting the employee.
                image_id = myReader.GetSqlGuid(0).ToString(); //.GetInt32(0).ToString();

                // Create a file to hold the output.
                fs = new FileStream(extractPath + image_id + ".zip",
                                    FileMode.OpenOrCreate, FileAccess.Write);
                bw = new BinaryWriter(fs);

                // Reset the starting byte for the new BLOB.
                startIndex = 0;

                // Read the bytes into outbyte[] and retain the number of bytes returned.
                retval = myReader.GetBytes(1, startIndex, outbyte, 0, bufferSize);

                // Continue reading and writing while there are bytes beyond the size of the buffer.
                while (retval == bufferSize)
                {
                    bw.Write(outbyte);
                    bw.Flush();

                    // Reposition the start index to the end of the last buffer and fill the buffer.
                    startIndex += bufferSize;
                    retval = myReader.GetBytes(1, startIndex, outbyte, 0, bufferSize);
                }

                // Write the remaining buffer.
                bw.Write(outbyte, 0, (int)retval);
                bw.Flush();

                FileInfo fileToDecompress = new FileInfo(fs.Name);

                // Close the output file.
                bw.Close();
                fs.Close();
    
                string currentZipName = fileToDecompress.FullName;
 
                using (ZipArchive archive = ZipFile.OpenRead(currentZipName))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        if (entry.FullName.EndsWith(".SAV", StringComparison.OrdinalIgnoreCase))
                        {
                            try
                            {
                                if (File.Exists(extractPath + entry.Name))
                                {
                                    File.Delete(extractPath + entry.Name);
                                }

                                entry.ExtractToFile(Path.Combine(extractPath, entry.Name));
                                FileInfo fi = new FileInfo(extractPath + entry.Name);

                                using (Bitmap image = new Bitmap(extractPath + entry.Name))
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                                        //image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                                        ms.WriteTo(Response.OutputStream);
                                    }
                                }
                                fi.Delete();
                            }
                            catch (Exception e)
                            {
                                Response.StatusCode.Equals(410);
                                Response.Write("Error in GetPatientImage " + "<br><br>");
                                Response.Write(e.Message + "<br><br>");
                                if (e.Message == "Block length does not match with its complement.") 
                                {
                                    Response.Write("the zip in the BLOB is likely password protected " + "<br><br>");
                                }
 //                               Response.Write(e + "<br>");
                            }
                        }
                    }
 
                }
                fileToDecompress.Delete();
            }
            // Close the reader and the connection.
            myReader.Close();
            connection.Close();
        }

        //TIFF2PDF impls
        public void MergeTiffs(string[] fileNames, string NewFileName, string outFile)
        {
            //combine into one tiff
            BitMiracle.TiffCP.Program.Main(fileNames);

            //convert the tiff to pdf
            string[] arguments = new string[] { "-o", outFile, "-p", "A4", NewFileName };
            BitMiracle.Tiff2Pdf.Program.Main(arguments);

            DisplayPDF(outFile);

            File.Delete(NewFileName);

        }

        private void createRandomTiff()
        {
            int width = 100;
            int height = 150;
            string fileName = "random.tif";

            using (Tiff output = Tiff.Open(fileName, "w"))
            {
                output.SetField(TiffTag.IMAGEWIDTH, width);
                output.SetField(TiffTag.IMAGELENGTH, height);
                output.SetField(TiffTag.SAMPLESPERPIXEL, 1);
                output.SetField(TiffTag.BITSPERSAMPLE, 8);
                output.SetField(TiffTag.ORIENTATION, BitMiracle.LibTiff.Classic.Orientation.TOPLEFT);
                output.SetField(TiffTag.ROWSPERSTRIP, height);
                output.SetField(TiffTag.XRESOLUTION, 88.0);
                output.SetField(TiffTag.YRESOLUTION, 88.0);
                output.SetField(TiffTag.RESOLUTIONUNIT, ResUnit.INCH);
                output.SetField(TiffTag.PLANARCONFIG, PlanarConfig.CONTIG);
                output.SetField(TiffTag.PHOTOMETRIC, Photometric.MINISBLACK);
                output.SetField(TiffTag.COMPRESSION, Compression.NONE);
                output.SetField(TiffTag.FILLORDER, FillOrder.MSB2LSB);

                Random random = new Random();
                for (int i = 0; i < height; ++i)
                {
                    byte[] buf = new byte[width];
                    for (int j = 0; j < width; ++j)
                        buf[j] = (byte)random.Next(255);

                    output.WriteScanline(buf, i);
                }

                output.WriteDirectory();
            }

            System.Diagnostics.Process.Start(fileName);
        }

        public void CombineMultiplePDFs(string[] fileNames, string outFile)
        {
            int pageOffset = 0;
            int f = 0;
            Document document = null;
            PdfCopy writer = null;
            while (f < fileNames.Length)
            {
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(fileNames[f]);
                reader.ConsolidateNamedDestinations();
                // we retrieve the total number of pages
                int n = reader.NumberOfPages;
                pageOffset += n;
                if (f == 0)
                {
                    // step 1: creation of a document-object
                    document = new Document(reader.GetPageSizeWithRotation(1));
                    // step 2: we create a writer that listens to the document
                    writer = new PdfCopy(document, new FileStream(outFile, FileMode.Create));
                    // step 3: we open the document
                    document.Open();
                }
                // step 4: we add content
                for (int i = 0; i < n; )
                {
                    ++i;
                    if (writer != null)
                    {
                        PdfImportedPage page = writer.GetImportedPage(reader, i);
                        writer.AddPage(page);
                    }
                }
                PRAcroForm form = reader.AcroForm;
                //this is tossing reader already added error. commenting allows something to be displayed
                //jbehrens 2/12/2016
                //if (form != null && writer != null)
                //{
                //    writer.AddDocument(reader);
                    //writer.CopyAcroForm(reader);
                //}
                f++;
            }
            // step 5: we close the document
            if (document != null)
            {
                document.Close();
            }

            DisplayPDF(outFile);

        }

        public void displayHTML(object htmlFilePath, string directoryPath)
        {

            //Read the Html File as Byte Array and Display it on browser
            byte[] bytes;
            using (FileStream fs = new FileStream(htmlFilePath.ToString(), FileMode.Open, FileAccess.Read))
            {
                BinaryReader reader = new BinaryReader(fs);
                bytes = reader.ReadBytes((int)fs.Length);
                fs.Close();
            }
            Response.BinaryWrite(bytes);
            Response.Flush();

            //Delete the Html File
            File.Delete(htmlFilePath.ToString());
            foreach (string file in Directory.GetFiles(directoryPath))
            {
                try
                {
                    File.Delete(file);
                }
                catch
                { }
            }
            //   Directory.Delete(directoryPath);
            Response.End();

        }

        public void DisplayPDF(String filename)
        {
            //String filefullname = Properties.Settings.Default.outpath + filename;
            Response.ContentType = "Application/PDF";

            object htmlFilePath = filename;
            string directoryPath = Server.MapPath("~/Temp/");


            displayHTML(htmlFilePath, directoryPath);

            //Delete the Uploaded PDF File
            File.Delete(filename);

        }


        [HandleProcessCorruptedStateExceptions]
        /// <summary>
        ///  // .. this memethod can be used to handle and log  Exceptions that are not handled by the CLR 
        /// </summary>


        public static void HandleCorruptedState()
        {
            // Write Exception Notification code here
            //  can log or send mail to admin etc.
        }

    }
}
